DIAGRAMS = diagrams
OUTPUT = output
BUNDLE = bundle.tar.gz

.PHONY: modules diagrams all clean bundle deploy

modules: $(OUTPUT)/libsodium.js $(OUTPUT)/libsodium-wrappers.js $(OUTPUT)/package.js

diagrams:
	cd $(DIAGRAMS) && $(MAKE) svg

all: modules diagrams

clean:
	cd $(DIAGRAMS) && $(MAKE) clean
	rm -rf $(OUTPUT)
	rm -f bundle.tar.gz

bundle: $(BUNDLE)

deploy: $(BUNDLE)
	cat $(BUNDLE) | ssh ktohg@tritarget.org "cd sandbox.tritarget.org/instant-chat && gunzip | tar x"

$(BUNDLE): diagrams modules index.html styles/* src/*
	tar c index.html styles src $(OUTPUT) | gzip > $(BUNDLE)

$(OUTPUT)/libsodium.js: node_modules/libsodium/dist/modules/libsodium.js
	@mkdir -p $(OUTPUT)
	cp $< $@

$(OUTPUT)/libsodium-wrappers.js: node_modules/libsodium-wrappers/dist/modules/libsodium-wrappers.js
	@mkdir -p $(OUTPUT)
	cp $< $@

$(OUTPUT)/package.js: package.json
	echo "export default" > $@
	cat $< >> $@
