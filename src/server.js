import EventEmitter from './event-emitter.js';
import { timeout } from './utils.js';

const MESSAGE_DELAY = 200;
const CONNECTION_DELAY = 600;

function randomId() {
  return 'xxxx'.replace(/[x]/g, function() {
    var r = Math.random() * 16 | 0;
    return r.toString(16);
  });
}

export default class Server extends EventEmitter {
  constructor() {
    super();
    this.clientsByAddress = new Map();
    this.clientsByLink = new Map();
    this.ephemeralKeys = new Map();
  }

  log(...values) {
    return this.emit('info', ...values);
  }

  postMeta(message) {
    return this.emit('meta', message);
  }

  findUnusedLink() {
    let link;
    do {
      link = [1, 2, 3, 4]
        .map(() => String.fromCharCode(Math.floor(Math.random() * 26) + 65))
        .join('');
    } while (this.clientsByLink.has(link));
    return link;
  }

  async registerClient(client, ephemeralPublicKey) {
    await timeout(CONNECTION_DELAY);
    let address = randomId();
    this.postMeta(`Registering address ${address}`);
    this.clientsByAddress.set(address, client);
    if (ephemeralPublicKey) {
      this.ephemeralKeys.set(client, ephemeralPublicKey);
    }
    return address;
  }

  async getShortUrlLink(client) {
    await timeout(CONNECTION_DELAY);
    let link = this.findUnusedLink();
    this.clientsByLink.set(link, client);
    this.postMeta(`Assigning link ${link} to client ${client.address}`);
    return link
  }

  async fetchClientInfo(link) {
    await timeout(CONNECTION_DELAY);
    if (!this.clientsByLink.has(link)) {
      throw new Error(`Link ${link} not found.`);
    }
    let client = this.clientsByLink.get(link);
    let address = client.address;
    let publicKey = this.ephemeralKeys.get(client);
    this.postMeta(`Client asked for public key of ${address}`);
    return { address, publicKey };
  }

  async post(event, address, payload) {
    await timeout(MESSAGE_DELAY);
    this.postMeta(`Recieved ${event} event for ${address}`);
    this.emit('ciphertext', payload);
    if (!this.clientsByAddress.has(address)) {
      throw new Error(`Address ${address} not found.`);
    }
    let client = this.clientsByAddress.get(address);
    if (client.link && /connect/.test(event)) {
      this.postMeta(`Link ${client.link} expiried`);
      this.clientsByLink.delete(client.link);
    }
    client.readMessage(event, payload);
  }
}
