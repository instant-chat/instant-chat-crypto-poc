import EventEmitter from './event-emitter.js';
import Sodium from './libsodium.js';

export default class Client extends EventEmitter {
  constructor(server) {
    super();
    this.server = server;
  }

  log(...args) {
    return this.emit('info', ...args);
  }

  postMeta(message) {
    return this.emit('meta', message);
  }

  postSend(message) {
    return this.emit('send', message);
  }

  postRecieve(message) {
    return this.emit('recieve', message);
  }

  sendMessage(message) {
    this.postToServer({ type: 'message', message });
    this.postSend(message);
  }

  readMessage(event, payload) {
    switch (event) {
      case 'connect-server': return this.establishServerSession(payload);
      case 'connect-client': return this.establishClientSession(payload);
      case 'message': return this.processMessage(payload);
      default: return this.postMeta(`recieved unknown ${event} message`);
    }
  }

  async initAsFirstClient() {
    this.ephemeralKeys = Sodium.crypto_box_keypair();
    this.address = await this.server.registerClient(this, Sodium.to_hex(this.ephemeralKeys.publicKey));
    this.link = await this.server.getShortUrlLink(this);
    this.postMeta(`Address: ${this.address}, Link: ${this.link}`);
  }

  async initAsSecondClient(link) {
    this.address = await this.server.registerClient(this);
    this.postMeta(`Address: ${this.address}`);
    let { address, publicKey } = await this.server.fetchClientInfo(link);
    this.recipientAddress = address;
    this.recipientEphemeralPublicKey = Sodium.from_hex(publicKey);
    this.authNonce = Sodium.randombytes_buf(Sodium.crypto_secretbox_NONCEBYTES);
    this.exchangeKeys = Sodium.crypto_kx_keypair();
    let message = JSON.stringify({
      address: this.address,
      publicKey: Sodium.to_hex(this.exchangeKeys.publicKey),
      nonce: Sodium.to_hex(this.authNonce)
    });
    this.log('crypto_box_seal', { message, recipientEphemeralPublicKey: this.recipientEphemeralPublicKey });
    let ciphertext = Sodium.crypto_box_seal(
      message,
      this.recipientEphemeralPublicKey
    );
    Sodium.increment(this.authNonce);
    this.postMeta('Waiting for key exchange');
    await this.server.post('connect-server', this.recipientAddress, Sodium.to_hex(ciphertext));
  }

  async establishServerSession(payload) {
    let json;
    try {
      let { publicKey, privateKey } = this.ephemeralKeys;
      let message = Sodium.from_hex(payload);
      this.log('crypto_box_seal_open', { message, publicKey, privateKey });
      let plaintext = Sodium.crypto_box_seal_open(message, publicKey, privateKey);
      json = JSON.parse(Sodium.to_string(plaintext));
    } catch(error) {
      this.log(error);
      return this.postMeta('Unable to decrypt message');
    }
    this.log('establishServerSession decrypted', json);
    this.recipientAddress = json.address;
    this.recipientPublicKey = Sodium.from_hex(json.publicKey);
    let nonce = Sodium.from_hex(json.nonce);
    Sodium.increment(nonce);
    this.exchangeKeys = Sodium.crypto_kx_keypair();
    let publicKey = Sodium.to_hex(this.exchangeKeys.publicKey);
    let plaintext = JSON.stringify({ publicKey });
    this.log('crypto_box_easy', { plaintext, nonce, recipientPublicKey: this.recipientPublicKey, privateExchangeKey: this.exchangeKeys.privateKey });
    let ciphertext = Sodium.crypto_box_easy(
      plaintext,
      nonce,
      this.recipientPublicKey,
      this.ephemeralKeys.privateKey
    );
    this.sharedKeys = Sodium.crypto_kx_server_session_keys(
      this.exchangeKeys.publicKey,
      this.exchangeKeys.privateKey,
      this.recipientPublicKey
    );
    this.postMeta('Wiping ephemeral keys');
    Sodium.memzero(this.ephemeralKeys.publicKey);
    Sodium.memzero(this.ephemeralKeys.privateKey);
    this.ephemeralKeys = null;
    await this.server.post('connect-client', this.recipientAddress, Sodium.to_hex(ciphertext));
    this.postMeta(`Connection established with client ${this.recipientAddress}`);
  }

  establishClientSession(payload) {
    let json;
    try {
      let message = Sodium.from_hex(payload);
      this.log('crypto_box_open_easy', { message, authNonce: this.authNonce, recipientEphemeralPublicKey: this.recipientEphemeralPublicKey, privateExcangeKey: this.exchangeKeys.privateKey });
      let plaintext = Sodium.crypto_box_open_easy(
        message,
        this.authNonce,
        this.recipientEphemeralPublicKey,
        this.exchangeKeys.privateKey
      );
      json = JSON.parse(Sodium.to_string(plaintext));
    } catch(error) {
      this.log(error);
      return this.postMeta('Bad message recieved');
    }
    this.log('establishClientSession decrypted', json);
    this.recipientPublicKey = Sodium.from_hex(json.publicKey);
    this.sharedKeys = Sodium.crypto_kx_client_session_keys(
      this.exchangeKeys.publicKey,
      this.exchangeKeys.privateKey,
      this.recipientPublicKey
    );
    this.postMeta(`Connection established with server ${this.recipientAddress}`);
  }

  async sendEncryptedMessage(payload) {
    let nonce = Sodium.randombytes_buf(Sodium.crypto_secretbox_NONCEBYTES);
    let message = JSON.stringify({ message: payload });
    this.log('crypto_secretbox_easy', { message, nonce, sharedKeyTx: this.sharedKeys.sharedTx });
    let ciphertext = Sodium.crypto_secretbox_easy(
      message,
      nonce,
      this.sharedKeys.sharedTx
    );
    let combinedMessage = `${Sodium.to_hex(nonce)}${Sodium.to_hex(ciphertext)}`;
    this.postSend(payload);
    await this.server.post('message', this.recipientAddress, combinedMessage);
  }

  processMessage(payload) {
    let json;
    let combinedMessage = Sodium.from_hex(payload);
    let nonce = combinedMessage.slice(0, Sodium.crypto_secretbox_NONCEBYTES);
    let ciphertext = combinedMessage.slice(Sodium.crypto_secretbox_NONCEBYTES);
    try {
      this.log('crypto_secretbox_open_easy', { ciphertext, nonce, sharedKeyRx: this.sharedKeys.sharedRx });
      let plaintext = Sodium.crypto_secretbox_open_easy(
        ciphertext,
        nonce,
        this.sharedKeys.sharedRx
      );
      json = JSON.parse(Sodium.to_string(plaintext));
      this.postRecieve(json.message);
    } catch(error) {
      this.log(error);
      this.postMeta('Bad message recieved');
    }
  }
}
