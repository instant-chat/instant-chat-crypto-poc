import Sodium from './libsodium.js';
import Server from './server.js';
import Client from './client.js';
import pkg from '../output/package.js'

function serializeUint8Array(value) {
  if (value instanceof Uint8Array) {
    return Sodium.to_hex(value);
  }
  if (value instanceof Object) {
    let result = {};
    for (let key of Object.keys(value)) {
      result[key] = serializeUint8Array(value[key]);
    }
    return result;
  }
  return value;
}

export default class Application {
  constructor() {
    this.charlie = new Server();
    this.charlieDom = {
      chat: document.getElementById('charlie-chat-box'),
    };
    this.alice = new Client(this.charlie);
    this.aliceDom = {
      chat: document.getElementById('alice-chat-box'),
      input: document.getElementById('alice-chat-input'),
      button: document.getElementById('alice-chat-send')
    };
    this.bob = new Client(this.charlie);
    this.bobDom = {
      chat: document.getElementById('bob-chat-box'),
      input: document.getElementById('bob-chat-input'),
      button: document.getElementById('bob-chat-send')
    };
    this.cipherTextDom = document.getElementById('cipher-text');
    this.link = {
      container: document.getElementById('link-container'),
      text: document.getElementById('link-text'),
      button: document.getElementById('link-button')
    };
    for (let el of document.querySelectorAll('[data-pkg]')) {
      let property = el.getAttribute('data-pkg');
      el.innerText = pkg[property];
    }
  }

  logMessage(type, outputEl, message) {
    let bubbleEl = document.createElement('div');
    bubbleEl.className = `chat-bubble ${type}`;
    bubbleEl.appendChild(document.createTextNode(message));
    outputEl.appendChild(bubbleEl);
    outputEl.scrollTop = outputEl.scrollHeight;
  }

  logCipherText(text) {
    let message = text.match(/.{1,32}/g).join('\n');
    this.cipherTextDom.innerHTML = '';
    this.cipherTextDom.appendChild(document.createTextNode(message));
  }

  logAlice(...args) {
    console.log('Alice:', ...args.map(serializeUint8Array));
  }

  logBob(...args) {
    console.log('Bob:', ...args.map(serializeUint8Array));
  }

  logCharlie() {
    console.log('Charlie:', ...arguments);
  }

  enableUnloadWarningMessage() {
    if (window.onbeforeunload) { return; }
    window.onbeforeunload = function(e) {
      return "Sure you want to leave?";
    };
  }

  aliceSendMessage() {
    let plaintext = this.aliceDom.input.value || 'lorem ipsum alice';
    this.aliceDom.input.value = '';
    this.alice.sendEncryptedMessage(plaintext);
    this.enableUnloadWarningMessage();
  }

  bobSendMessage() {
    let plaintext = this.bobDom.input.value || 'lorem ipsum bob';
    this.bobDom.input.value = '';
    this.bob.sendEncryptedMessage(plaintext);
    this.enableUnloadWarningMessage();
  }

  showLink(link) {
    this.link.button.disabled = false;
    this.link.text.appendChild(document.createTextNode(`https://example.com/${link}`));
  }

  hideLink() {
    this.link.container.className = 'hidden';
  }

  enableClientForms() {
    this.bobDom.input.disabled = false;
    this.bobDom.button.disabled = false;
    this.aliceDom.input.disabled = false;
    this.aliceDom.button.disabled = false;
  }

  async start() {
    this.alice.on('info', (...args) => this.logAlice(...args));
    this.bob.on('info', (...args) => this.logBob(...args));
    this.charlie.on('info', (...args) => this.logCharlie(...args));
    this.charlie.on('ciphertext', text => this.logCipherText(text));
    ['meta', 'send', 'recieve'].forEach(type => {
      this.alice.on(type, msg => this.logMessage(type, this.aliceDom.chat, msg));
      this.bob.on(type, msg => this.logMessage(type, this.bobDom.chat, msg));
      this.charlie.on(type, msg => this.logMessage(type, this.charlieDom.chat, msg));
    });
    this.aliceDom.input.addEventListener('keyup', event => {
      if (event.keyCode !== 13) { return; }
      event.preventDefault();
      this.aliceSendMessage();
    });
    this.aliceDom.button.addEventListener('click', () => this.aliceSendMessage());
    this.bobDom.input.addEventListener('keyup', event => {
      if (event.keyCode !== 13) { return; }
      event.preventDefault();
      this.bobSendMessage();
    });
    this.bobDom.button.addEventListener('click', () => this.bobSendMessage());
    this.link.button.addEventListener('click', () => this.connectClients());
    await Sodium.ready;
    await this.bob.initAsFirstClient();
    this.showLink(this.bob.link);
  }

  connectClients() {
    this.hideLink();
    this.enableClientForms();
    this.alice.initAsSecondClient(this.bob.link)
  }
}
